use anchor_lang::prelude::*;
use anchor_lang::solana_program::entrypoint::ProgramResult;
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::system_instruction::transfer;


declare_id!("8c8vRN2xwWFKdk4DpwBLmVYrbvRPNymLAPCx4JM4zdC6");

#[program]
pub mod crypton {
    use super::*;
    pub fn initialize(ctx: Context<Init>) -> ProgramResult {
        let base_account = &mut ctx.accounts.base_account;
        base_account.owner = "GaazCDLpR2JyDmYyxoT9r7faY8DectQdbSYfTzdSsRhb".parse().unwrap();
        Ok(())
    }

    pub fn send_donation(ctx: Context<Donation>, amount: u64) -> Result<()> {
        require!(amount > 0, DonationError::ZeroAmountForbidden);

        let base_account = &mut ctx.accounts.base_account;
        let user = &ctx.accounts.user;

        match base_account
            .donators
            .iter_mut()
            .find(|item| item.user == user.key())
        {
            Some(item_found) => {
                item_found.amount += amount;
            }
            None => base_account.donators.push(Donators {
                user: user.key(),
                amount,
            }),
        }

        invoke(
            &transfer(&user.key(), &base_account.key(), amount),
            &[user.to_account_info(), base_account.to_account_info()],
        ).unwrap();
        Ok(())
    }

    pub fn withdraw(ctx: Context<Withdrawal>) -> ProgramResult {
        let base_account = &ctx.accounts.base_account.to_account_info();

        let owner = &ctx.accounts.owner;

        **owner.try_borrow_mut_lamports()? += base_account.lamports();
        **base_account.try_borrow_mut_lamports()? = 0;
        Ok(())
    }
}

#[derive(Accounts)]
pub struct Init<'info> {
    #[account(init, payer = user, space = 64 + 1024)]
    pub base_account: Account<'info, BaseAccount>,

    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[account]
pub struct BaseAccount {
    pub donators: Vec<Donators>,
    pub owner: Pubkey,
}

#[derive(Accounts)]
pub struct Donation<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,

    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct Withdrawal<'info> {
    #[account(mut, signer)]
    pub base_account: Account<'info, BaseAccount>,

    #[account(address = base_account.owner)]
    pub owner: Signer<'info>,
}

#[derive(Clone, Debug, AnchorSerialize, AnchorDeserialize)]
pub struct Donators {
    user: Pubkey,
    amount: u64,
}

#[error_code]
pub enum DonationError {
    ZeroAmountForbidden,
}  
