import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { expect } from "chai";
import { Crypton } from "../target/types/crypton";

describe("crypton", () => {
  const provider = anchor.Provider.env();
  anchor.setProvider(provider);

  const program = anchor.workspace.Crypton as Program<Crypton>;

  const baseAccount = anchor.web3.Keypair.generate();
  const user = anchor.web3.Keypair.generate();

  const sendDonation = async (user: anchor.web3.Keypair) => {
    await program.rpc.sendDonation(new anchor.BN(2), {
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: user.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      },
      signers: [user],
    });
  }

  const requestAndConfirmAirdrop = async (provider: anchor.Provider, user: anchor.web3.Keypair, amount: number) => {
    return await provider.connection.confirmTransaction(
      await provider.connection.requestAirdrop(user.publicKey, amount),
      "confirmed");
  }


  it("It creates an account!", async () => {
    const tx = await program.rpc.initialize({
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      },
      signers: [baseAccount],
    });
    console.log("your transaction signature", tx);

    const account = await program.account.baseAccount.fetch(
      baseAccount.publicKey
    );
    expect(account.owner).to.deep.equal(provider.wallet.publicKey);
  });

  it("Sends a donation!", async () => {
    await requestAndConfirmAirdrop(provider, user, 2_000_000);

    await sendDonation(user)

    const balance = await program.account.baseAccount.getAccountInfo(
      user.publicKey
    );
    expect(balance.lamports).to.greaterThan(1_000_000);
  });

  it("Returns the list of donators!", async () => {
    const donator = anchor.web3.Keypair.generate();

    await requestAndConfirmAirdrop(provider, donator, 2_000_000);
    await sendDonation(donator);


    const account = await program.account.baseAccount.fetch(
      baseAccount.publicKey
    );

    expect((account.donators as []).length).to.equal(2);
    expect(account.donators[0].user).to.deep.equal(user.publicKey);
    expect(account.donators[1].user).to.deep.equal(donator.publicKey);
  });

  it("Withdraws to baseAccount!", async () => {
    const balanceBefore = await provider.connection.getBalance(provider.wallet.publicKey);

    await program.rpc.withdraw({
      accounts: {
        baseAccount: baseAccount.publicKey,
        owner: provider.wallet.publicKey,
      },
      signers: [baseAccount],
    });

    const balanceAfter = await provider.connection.getBalance(provider.wallet.publicKey);
    expect(balanceAfter).to.greaterThan(balanceBefore);
  });
});
