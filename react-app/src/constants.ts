import {
  clusterApiUrl,
  Keypair,
  PublicKey,
} from "@solana/web3.js";
import idl from "./crypton.json";
import { WalletAdapterNetwork } from "@solana/wallet-adapter-base";
import { SolletExtensionWalletAdapter } from "@solana/wallet-adapter-sollet";
import {ConfirmOptions} from "./types";

let seed = Uint8Array.from([67,239,186,221,37,228,239,201,140,36,253,67,255,70,11,188,231,83,36,210,54,171,227,196,135,231,123,136,240,25,228,162,231,121,230,23,47,36,132,191,128,125,195,199,188,178,145,197,220,98,112,121,158,106,101,129,1,11,116,51,88,182,70,178]);
// let seed = Uint8Array.from([221,110,192,87,243,38,14,185,70,235,146,237,57,33,148,89,55,10,240,49,59,226,35,103,167,191,204,20,42,216,115,80,114,141,17,176,252,137,99,85,254,74,2,157,249,235,230,199,186,192,109,145,198,234,152,226,183,241,30,12,42,70,27,198]);
export const baseAccount = Keypair.fromSecretKey(seed);

export const programID = new PublicKey(idl.metadata.address);
export const network = clusterApiUrl("devnet") as WalletAdapterNetwork;
export const owner = new PublicKey(
  "GaazCDLpR2JyDmYyxoT9r7faY8DectQdbSYfTzdSsRhb"
);
// export const network = "http://127.0.0.1:8899" as WalletAdapterNetwork;
export const wallets = [new SolletExtensionWalletAdapter({ network })];

export const opts: ConfirmOptions = {
  preflightCommitment: "confirmed",
};
