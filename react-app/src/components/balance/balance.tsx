import React, { memo, useEffect } from "react";
import styles from "./balance.module.css";
import { useProgram } from "../../hooks/use-program";

const BalanceComponent = () => {
  const { balance, updateBalance } = useProgram();

  useEffect(() => {
    updateBalance();
  }, [updateBalance]);

  return (
    <header>
      {balance ? (
        <div className={styles.balance}>
          <div className={styles.amount}>{balance}</div>
          <span>total donated</span>
        </div>
      ) : (
        <h1 className={styles.title}>Crypton</h1>
      )}
    </header>
  );
};

export const Balance = memo(BalanceComponent);
